resource "google_storage_bucket" "static-site" {
  name          = format("image-store-%s", local.uuid)
  location      = "EU"
  force_destroy = true

  bucket_policy_only = true
}

output "env-dynamic-url" {
  value = format("https://image-store-%s.storage.googleapis.com/", local.uuid)
}
