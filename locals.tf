locals {
  uuid = split("-", random_uuid.uuid.id)[4]
}

resource "random_uuid" "uuid" {}
